# simple-controller

Arduino sketch and python reader for simple controller.

<img src="https://github.com/minirobots/simple-controller/blob/master/kids-controller.png" width="800px" />

<img src="https://github.com/minirobots/simple-controller/blob/master/simple-controller.png" width="800px" />
