/**
 * Author: Leo Vidarte <http://minirobots.com.ar>
 *
 * This is free software,
 * you can redistribute it and/or modify it
 * under the terms of the GPL version 3
 * as published by the Free Software Foundation.
 */

const int pinBuzzer = 13;
const int freqButtons[] = {1760, 2093, 2637, 2794};
const int pinButtons[] = {2, 3, 4, 5};
int stateButtons[] = {0, 0, 0, 0};

int totalButtons()
{
  return sizeof(pinButtons) / sizeof(int);
}

void setup()
{
  Serial.begin(9600);
  pinMode(pinBuzzer, OUTPUT);

  for (int i = 0; i < totalButtons(); i++)
  {
    pinMode(pinButtons[i], INPUT);
  }
}

void checkButton(int button)
{
  if (digitalRead(pinButtons[button]) == HIGH)
  {
    if (stateButtons[button] == 0)
    {
      stateButtons[button] = 1;
      Serial.write(button);
      tone(pinBuzzer, freqButtons[button], 100);
    }
  }
  else
  {
    stateButtons[button] = 0;
  }
}

void loop()
{
  for (int i = 0; i < totalButtons(); i++)
  {
    checkButton(i);
  }

  delay(100); // Delay a little bit to avoid bouncing
}
