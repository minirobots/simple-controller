"""
Author: Leo Vidarte <http://minirobots.com.ar>

This is free software,
you can redistribute it and/or modify it
under the terms of the GPL version 3
as published by the Free Software Foundation.

"""

import sys
import time
from serial import Serial
from minirobots import Turtle

controller = Serial(sys.argv[1], 9600)

if len(sys.argv) == 3:
    turtle = Turtle(Serial(sys.argv[2], 57600))
    turtle.pen_down()
else:
    turtle = None

commands = ['forward', 'right', 'left', 'backward']
values = [27, 90, 90, 27]

while True:
    command = int(ord(controller.read()))
    print(command, commands[command])
    if turtle:
        getattr(turtle, commands[command])(values[command])

